#include <cmath>
#include "Presentation.h"
#include "Fortune.h"

const int BEACH_LINE_EXPAND = 1000;

/// \brief Holds float coordinates of a node
struct f_Node {
    float x;
    float y;
};

/// \brief Holds a circle
struct Circle {
    f_Node centre;
    float radius;
};

/// \brief Describes circle event
struct CE {
    Circle circle;
    Node a;
    Node b;
    Node c;
    float y;
};

/// \brief Converts f_Node to Node by flooring its coordinates
Node floor_f_Node(const f_Node a) {
    return {(int)a.x, (int)a.y};
}

/// \brief Gets a circle with three given points
Circle three_point_circle(const Node a, const Node b, const Node c) {
    float delta_xab = a.x - b.x;
    float delta_yab = a.y - b.y;
    float delta_xac = a.x - c.x;
    float delta_yac = a.y - c.y;

    float xxac = a.x * a.x - c.x * c.x;
    float yyac = a.y * a.y - c.y * c.y;
    float xxab = a.x * a.x - b.x * b.x;
    float yyab = a.y * a.y - b.y * b.y;

    float f = (xxac * delta_xab + yyac * delta_xab - xxab * delta_xac - yyab * delta_xac) / (2 * (delta_yab * delta_xac - delta_yac * delta_xab)) * -1;
    float g = (xxac * delta_yab + yyac * delta_yab - xxab * delta_yac - yyab * delta_yac) / (2 * (delta_xab * delta_yac - delta_xac * delta_yab)) * -1;
    float h = 2 * f * a.y + 2 * g * a.x - a.x * a.x - a.y * a.y;

    Circle result;
    result.centre = {g, f};
    result.radius = sqrt(f * f + g * g - h);

    return result;
}

/// \brief Returns if the node is in the circle
bool node_in_circle(const Node n, const Circle o) {
    float n_o = sqrt((o.centre.x - n.x) * (o.centre.x - n.x) + (o.centre.y - n.y) * (o.centre.y - n.y));
    return n_o < o.radius;
}

/// \brief Returns a visible part of the beach line at the given y coordinate
Line beach_line(const float y) {
    Node left = {Plane::get_bbox().min_x - BEACH_LINE_EXPAND, (int)y};
    Node right = {Plane::get_bbox().max_x + BEACH_LINE_EXPAND, (int)y};

    Line result = {left, right};
    return result;
}

/// \brief Says if the node is in CE
bool is_in_ce(Node n, CE ce) {
    return n.x == ce.a.x && n.y == ce.a.y || n.x == ce.b.x && n.y == ce.b.y || n.x == ce.c.x && n.y == ce.c.y;
}

void fortune() {
    std::vector<Line> voronoi;
    Pr::new_slide();
    int i, j, k, l;

    // Find circle events
    std::vector<CE> circle_events, voronoi_vertexes;
    for (i = 0; i < Plane::size() - 2; i++) {
        for (j = i + 1; j < Plane::size() - 1; j++) {
            for (k = j + 1; k < Plane::size(); k++) {
                Circle o = three_point_circle(Plane::n(i), Plane::n(j), Plane::n(k));
                bool empty_o = true;

                for (l = 0; l < Plane::size(); l++) {
                    if (l == i || l == j || l == k) {
                        continue;
                    }
                    if (node_in_circle(Plane::n(l), o)) {
                        empty_o = false;
                        break;
                    }
                }
                if (empty_o) {
                    CE new_ce;
                    new_ce.circle = o;
                    new_ce.a = Plane::n(i);
                    new_ce.b = Plane::n(j);
                    new_ce.c = Plane::n(k);
                    new_ce.y = o.centre.y + o.radius;
                    circle_events.push_back(new_ce);
                }
            }
        }
    }

    // Sort site events by y coordinate
    std::vector<Node> site_events;

    for (i = 0; i < Plane::size(); i++) {
        site_events.push_back(Plane::n(i));
    }

    bool changed = true;
    while (changed) {
        changed = false;

        for (i = 1; i < site_events.size(); i++) {
            if (site_events[i - 1].y > site_events[i].y) {
                changed = true;
                Node tmp = site_events[i - 1];
                site_events[i - 1] = site_events[i];
                site_events[i] = tmp;
            }
        }
    }

    // Sort circle events by y coordinate
    changed = true;
    while (changed) {
        changed = false;

        for (i = 1; i < circle_events.size(); i++) {
            if (circle_events[i - 1].y > circle_events[i].y) {
                changed = true;
                CE tmp = circle_events[i - 1];
                circle_events[i - 1] = circle_events[i];
                circle_events[i] = tmp;
            }
        }
    }

    // Move beach line
    while (! (site_events.empty() && circle_events.empty())) {
        Pr::new_slide();
        float bl_y;
        if ((site_events.empty() && (! circle_events.empty())) || (circle_events[0].y < site_events[0].y)) {
            bl_y = circle_events[0].y;
            Pr::back()->add_line({circle_events[0].a, floor_f_Node(circle_events[0].circle.centre)});
            Pr::back()->add_line({circle_events[0].b, floor_f_Node(circle_events[0].circle.centre)});
            Pr::back()->add_line({circle_events[0].c, floor_f_Node(circle_events[0].circle.centre)});
            Pr::back()->add_line({floor_f_Node({circle_events[0].circle.centre.x, bl_y}), floor_f_Node(circle_events[0].circle.centre)});

            for (i = 0; i < voronoi_vertexes.size(); i++) {
                if ((is_in_ce(circle_events[0].a, voronoi_vertexes[i]) && is_in_ce(circle_events[0].b, voronoi_vertexes[i])) ||
                (is_in_ce(circle_events[0].a, voronoi_vertexes[i]) && is_in_ce(circle_events[0].c, voronoi_vertexes[i])) ||
                (is_in_ce(circle_events[0].b, voronoi_vertexes[i]) && is_in_ce(circle_events[0].c, voronoi_vertexes[i]))) {
                    voronoi.push_back({floor_f_Node(circle_events[0].circle.centre), floor_f_Node(voronoi_vertexes[i].circle.centre)});
                }
            }

            voronoi_vertexes.push_back(circle_events[0]);
            circle_events.erase(circle_events.begin());
        } else {
            bl_y = site_events[0].y;
            site_events.erase(site_events.begin());
        }
        Pr::back()->add_line(beach_line(bl_y));
        Pr::back()->add_lines(voronoi);
    }

    Pr::new_slide();
    Pr::back()->add_lines(voronoi);
}