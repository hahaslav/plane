#include "Presentation.h"
#include "ConvexHull.h"

const Node LEFT_NODE {MINF, -1};
const Node RIGHT_NODE {INF, -1};

void kirkpatrick() {
    if (Plane::get_bbox().min_y < 0) {
        throw("Nodes with negative y coordinate are not supported");
    }
    std::vector<Node> left(Plane::get_bbox().max_y + 1, RIGHT_NODE);
    std::vector<Node> right(Plane::get_bbox().max_y + 1, LEFT_NODE);
    int i;

    // Sort nodes
    for (i = 0; i < Plane::size(); i++) {
        if (Plane::n(i).x < left[Plane::n(i).y].x) {
            left[Plane::n(i).y] = Plane::n(i);
        }
        if (Plane::n(i).x > right[Plane::n(i).y].x) {
            right[Plane::n(i).y] = Plane::n(i);
        }
    }

    // Make left side
    std::vector<Line> left_lines;
    Node first_node = LEFT_NODE;
    for (i = 0; i < left.size(); i++) {
        if (left[i].y == -1) {
            continue;
        }
        Pr::new_slide();

        if (first_node.y == -1) {
            first_node = left[i];
        } else {
            while ((! left_lines.empty()) && angle_direction(left_lines.back().A, left_lines.back().B, left[i]) == COUNTERCLOCKWISE_ANGLE) {
                left_lines.pop_back();
            }
            if (left_lines.empty()) {
                left_lines.push_back({first_node, left[i]});
            } else {
                left_lines.push_back({left_lines.back().B, left[i]});
            }
        }

        Pr::back()->add_lines(left_lines);
    }

    // Make right side
    std::vector<Line> right_lines;
    first_node = LEFT_NODE;
    for (i = 0; i < right.size(); i++) {
        if (right[i].y == -1) {
            continue;
        }
        if (first_node.y == -1) {
            first_node = right[i];
            continue;
        }
        Pr::new_slide();
        Pr::back()->add_lines(left_lines);

        while ((! right_lines.empty()) && angle_direction(right_lines.back().A, right_lines.back().B, right[i]) == CLOCKWISE_ANGLE) {
            right_lines.pop_back();
        }
        if (right_lines.empty()) {
            right_lines.push_back({first_node, right[i]});
        } else {
            right_lines.push_back({right_lines.back().B, right[i]});
        }

        Pr::back()->add_lines(right_lines);
    }

    // Make top and bottom sides, final slide
    Pr::new_slide();
    Pr::back()->add_lines(left_lines);
    Pr::back()->add_lines(right_lines);
    Pr::back()->add_line({left_lines[0].A, right_lines[0].A});
    Pr::back()->add_line({left_lines.back().B, right_lines.back().B});
}