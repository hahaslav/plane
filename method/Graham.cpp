#include "Presentation.h"
#include "ConvexHull.h"

/// \brief Node with its angle
struct NA {
    Node n;
    float a;
};

void graham() {
    Pr::new_slide();
    int i;

    // Find central node
    BBOX bbox = Plane::get_bbox();
    Node centre = {(bbox.min_x + bbox.max_x) / 2, (bbox.min_y + bbox.max_y) / 2};
    Pr::new_slide();
    Pr::back()->add_node(centre);

    // Sort nodes by angle from the central node
    std::vector<NA> nodangles;

    for (i = 0; i < Plane::size(); i++) {
        NA new_na = {Plane::n(i), angle(centre, Plane::n(i))};
        nodangles.push_back(new_na);
    }

    bool changed = true;
    while (changed) {
        changed = false;
        for (i = 1; i < nodangles.size(); i++) {
            if (nodangles[i - 1].a > nodangles[i].a) {
                changed = true;
                NA tmp = nodangles[i - 1];
                nodangles[i - 1] = nodangles[i];
                nodangles[i] = tmp;
            }
        }
    }

    // Make convex hull
    std::vector<Line> lines;
    for (i = 1; i < nodangles.size(); i++) {
        Pr::new_slide();

        while ((! lines.empty()) && angle_direction(lines.back().A, lines.back().B, nodangles[i].n) == CLOCKWISE_ANGLE) {
            lines.pop_back();
        }
        if (lines.empty()) {
            lines.push_back({nodangles[0].n, nodangles[i].n});
        } else {
            lines.push_back({lines.back().B, nodangles[i].n});
        }

        Pr::back()->add_lines(lines);
    }
    while ((! lines.empty()) && angle_direction(lines.back().A, lines.back().B, nodangles[0].n) == CLOCKWISE_ANGLE) {
        lines.pop_back();
    }
    if (! lines.empty()) {
        lines.push_back({lines.back().B, nodangles[0].n});
    }

    Pr::new_slide();
    Pr::back()->add_lines(lines);
}