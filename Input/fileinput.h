#ifndef LAB3_FILEINPUT_H
#define LAB3_FILEINPUT_H
/**
 * \brief Reads nodes coordinates from file with given filepath
 *
 * In-file format of node's coordinates: x y. One node per line.
 */
void load_nodes_from_file(const std::string &filepath);
#endif //LAB3_FILEINPUT_H
