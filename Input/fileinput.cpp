#include <fstream>
#include <regex>
#include "fileinput.h"
#include "Geometry.h"

const std::regex coords_regex("(-?\\d+) (-?\\d+)");

/// \brief Converts std::string into int
int string_to_int(std::string s) {
    int result = 0, i = 0;
    bool to_invert = false;
    if (s[0] == '-') {
        to_invert = true;
        i = 1;
    }

    for (i; s[i] != 0; i++) {
        result *= 10;
        result += s[i] - '0'; // it is converted from the ASCII character code
    }

    if (to_invert) {
        result *= -1;
    }
    return result;
}

void load_nodes_from_file(const std::string &filepath) {
    std::ifstream fin (filepath);
    std::vector<Node> result;

    while(!fin.eof()) {
        std::string line;
        std::smatch match;

        getline(fin, line);
        std::regex_match(line, match, coords_regex);
        Node new_node{string_to_int(match[1].str()), string_to_int(match[2].str())};
        result.push_back(new_node);
    }
    fin.close();

    Plane::set_data(result);
}