#include <vector>

#ifndef LAB3_GEOMETRY_H
#define LAB3_GEOMETRY_H
const int CLOCKWISE_ANGLE = -1;
const int COUNTERCLOCKWISE_ANGLE = 1;
const int STRAIGHT_ANGLE = 0;

const int INF = 0x7FFFFFFF; // maximal int = 2^31 - 1 = 2147483647
const int MINF = 0x80000000; // minimal int = -(2^31) = -2147483648

/// \brief Holds coordinates of a node
struct Node {
    int x;
    int y;
};

/// \brief Holds the ends of a line
struct Line {
    Node A;
    Node B;
};

/// \brief Holds the bounding box
struct BBOX {
    int min_x;
    int min_y;
    int max_x;
    int max_y;
};

/// \brief Static class that holds loaded set of nodes
class Plane {
    Plane() {} // to prevent creation of new instances of the Plane class
public:
    /// \brief Accepts a vector of Nodes for further work
    static void set_data(const std::vector<Node> &new_data);

    /// \brief Returns the number of loaded nodes
    static int size();

    /// \brief Getter for bbox of the nodes
    static BBOX get_bbox();

    /// \brief Getter for loaded nodes
    static Node n(const int i);
};

/**
 * \brief Returns a direction of an angle ABC
 * relatively to the clock
 */
int angle_direction(const Node &a, const Node &b, const Node &c);

/// \brief Returns distance between two nodes
float dist(const Node &a, const Node &b);

/// \brief Returns clockwise angle between vector ab and axis x, in radian
float angle(const Node &a, const Node &b);
#endif //LAB3_GEOMETRY_H