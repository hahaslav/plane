#include <cmath>
#include "Geometry.h"
#include "Slide.h"

std::vector<Node> inputted_data;
BBOX data_bbox;

/// \brief Finds the edge nodes of the inputted data
void make_bbox() {
    data_bbox = {INF, INF, MINF, MINF};
    int n = inputted_data.size();
    int i;

    for (i = 0; i < n; i++) {
        if (inputted_data[i].x < data_bbox.min_x) {
            data_bbox.min_x = inputted_data[i].x;
        }
        if (inputted_data[i].y < data_bbox.min_y) {
            data_bbox.min_y = inputted_data[i].y;
        }
        if (inputted_data[i].x > data_bbox.max_x) {
            data_bbox.max_x = inputted_data[i].x;
        }
        if (inputted_data[i].y > data_bbox.max_y) {
            data_bbox.max_y = inputted_data[i].y;
        }
    }
}

void Plane::set_data(const std::vector<Node> &new_data) {
    inputted_data = new_data;
    make_bbox();
    setup_projector();
}

int Plane::size() {
    return inputted_data.size();
}

BBOX Plane::get_bbox() {
    return data_bbox;
}

Node Plane::n(const int i) {
    return inputted_data.at(i);
}

int angle_direction(const Node &a, const Node &b, const Node &c) {
    int left_part = c.y * (b.x - a.x);
    int right_part = (c.x - a.x) * (b.y - a.y) + a.y * (b.x - a.x);
    if (left_part > right_part) {
        return COUNTERCLOCKWISE_ANGLE;
    }
    if (left_part == right_part) {
        return STRAIGHT_ANGLE;
    }
    return CLOCKWISE_ANGLE;
}

float dist(const Node &a, const Node &b) {
    return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

float angle(const Node &a, const Node &b) {
    float result = acos((b.x - a.x) / dist(a, b));
    if (b.y < a.y) {
        result = 2 * M_PI - result;
    }
    return result;
}