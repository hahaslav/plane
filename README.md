 * [X] Fortune algorithm of creating a Voronoi diagrams
 * [ ] Delaunay triangulation
 * [X] Kirkpatrick algorithm
 * [ ] Andrew and Javers algorithm
 * [X] Graham algorithm
 * [ ] Fast algorithm