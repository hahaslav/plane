#include "Slide.h"

const int MARGIN = 50;
const int MARKER_SIZE = 4;

int project_x_shift = 0;
float project_x_stretch = 1;
int project_y_shift = 0;
float project_y_stretch = 1;

float project_x(const int x) {
    return (x - project_x_shift) * project_x_stretch + MARGIN;
}
float project_y(const int y) {
    return (y - project_y_shift) * project_y_stretch + MARGIN;
}

int Slide::size() const {
    return objects.size();
}

sf::VertexArray Slide::operator[](const int i) const {
    return objects.at(i);
}

void Slide::add_node(const Node a)
{
    sf::VertexArray marker(sf::LineStrip, 5);
    marker[0].position = sf::Vector2f(project_x(a.x) - MARKER_SIZE, project_y(a.y) - MARKER_SIZE);
    marker[1].position = sf::Vector2f(project_x(a.x) + MARKER_SIZE, project_y(a.y) + MARKER_SIZE);
    marker[2].position = sf::Vector2f(project_x(a.x), project_y(a.y));
    marker[3].position = sf::Vector2f(project_x(a.x) - MARKER_SIZE, project_y(a.y) + MARKER_SIZE);
    marker[4].position = sf::Vector2f(project_x(a.x) + MARKER_SIZE, project_y(a.y) - MARKER_SIZE);
    objects.push_back(marker);
}

void Slide::add_line(const Line a)
{
    sf::VertexArray line(sf::LineStrip, 2);
    line[0].position = sf::Vector2f(project_x(a.A.x), project_y(a.A.y));
    line[1].position = sf::Vector2f(project_x(a.B.x), project_y(a.B.y));
    objects.push_back(line);
}

void Slide::add_lines(const std::vector<Line> &a)
{
    int i;

    for (i = 0; i < a.size(); i++) {
        add_line(a[i]);
    }
}

void setup_projector() {
    BBOX bbox = Plane::get_bbox();

    project_x_shift = bbox.min_x;
    project_y_shift = bbox.min_y;

    project_x_stretch = (float)(WINDOW_WIDTH - 2 * MARGIN) / (bbox.max_x - project_x_shift);
    project_y_stretch = (float)(WINDOW_HEIGHT - 2 * MARGIN) / (bbox.max_y - project_y_shift);
}