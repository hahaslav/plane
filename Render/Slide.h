#include <SFML/Graphics.hpp>
#include "Geometry.h"

#ifndef LAB3_SLIDE_H
#define LAB3_SLIDE_H
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 800;

class Slide {
    std::vector<sf::VertexArray> objects;
public:
    Slide() {}

    /// \brief Returns the number of objects on the slide
    int size() const;

    /// \brief Getter for one element from the slide
    sf::VertexArray operator[](const int i) const;

    /// \brief Adds an X marker to the slide
    void add_node(const Node a);

    /// \brief Adds a line to the slide
    void add_line(const Line a);

    /// \brief Adds lines to the slide
    void add_lines(const std::vector<Line> &a);
};

/**
 * \brief Sets projection params based on the inputted nodes
 *
 * Projection stretches the nodes within the window
 * including margins from all sides.
 */
void setup_projector();
#endif //LAB3_SLIDE_H