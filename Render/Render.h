#ifndef LAB3_RENDER_H
#define LAB3_RENDER_H
/// \brief API wrapper for SFML
class Render {
    Render() {} // to prevent creation of new instances of the Render class
public:
    /// \brief Opens a new window
    static void open_window();

    /**
     * \brief Tells whether or not the window is open
     *
     * \see sf::Window::isOpen()
     */
    static bool isOpen();

    /// \brief Checks if the user wants to close the window
    static void check_closing();

    /// \brief Draws all objects of the current slide
    static void draw();

    /**
     * \brief Changes slide if user presses a mouse button
     *
     * LMB - next slide, RMB - previous slide.
     */
    static void check_change_slide();
};
#endif //LAB3_RENDER_H