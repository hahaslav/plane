#include "Slide.h"

#ifndef LAB3_PRESENTATION_H
#define LAB3_PRESENTATION_H
/**
 * \brief Static class that holds the sequence of Slides
 *
 * Full name: Presentation
 */
class Pr {
    Pr() {} // to prevent creation of new instances of the Pr class
public:
    /// \brief Returns the number of slides in the presentation
    static int length();

    /// \brief Getter for slides
    static Slide slide(const int i);

    /// \brief Returns a pointer to the last slide
    static Slide* back();

    /**
     * \brief Creates a new slide at the end of the presentation,
     * puts the inputted Nodes into this slide
     */
     static void new_slide();
};

#endif //LAB3_PRESENTATION_H