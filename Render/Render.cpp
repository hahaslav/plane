#include <SFML/Window/Mouse.hpp>
#include "Presentation.h"
#include "Render.h"

int current_slide;
sf::RenderWindow window;

/**
 * \brief Returns currently pressed mouse button
 *
 *  If no buttons pressed, returns (-1)MB.
 * Can detect only LMB and RMB.
 * If two buttons are pressed simultaneously,
 * returns LMB
 */
sf::Mouse::Button get_mouse_button() {
    if (window.hasFocus()) {
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            return sf::Mouse::Left;
        }
        if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
            return sf::Mouse::Right;
        }
    }
    return sf::Mouse::Button(-1); // not 0 because it is LMB
}

void Render::open_window() {
    window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "lab3");
    window.requestFocus();
}

bool Render::isOpen() {
    return window.isOpen();
}

void Render::check_closing() {
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window.close();
    }
}

void Render::draw() {
    int i;

    window.clear();
    for (i = 0; i < Pr::slide(current_slide).size(); i++) {
        window.draw(Pr::slide(current_slide)[i]);
    }
    window.display();
}

void Render::check_change_slide() {
    sf::Mouse::Button mb_clicked = get_mouse_button();
    switch (mb_clicked) {
        case sf::Mouse::Left: {
            if (current_slide + 1 < Pr::length()) {
                current_slide++;
            }
            break;
        }
        case sf::Mouse::Right: {
            if (current_slide > 0) {
                current_slide--;
            }
            break;
        }
        default:
            return;
    }

    // prevent further executions while holding the button
    while (get_mouse_button() != sf::Mouse::Button(-1));
}