#include "Presentation.h"

std::vector<Slide> slides;

int Pr::length() {
    return slides.size();
}

Slide Pr::slide(const int i) {
    return slides.at(i);
}

Slide* Pr::back() {
    return &slides.back();
}

void Pr::new_slide() {
    slides.push_back(Slide());
    int i;

    for (i = 0; i < Plane::size(); i++) {
        back()->add_node(Plane::n(i));
    }
}