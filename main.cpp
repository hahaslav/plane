#include <iostream>
#include "Render/Render.h"
#include "method/ConvexHull.h"
#include "method/Fortune.h"
#include "Input/fileinput.h"

/**
 * \brief Gets filepath from std::cin
 *
 * Asks a user to enter the path to a file that
 * has coordinates of the nodes to work with.
 */
std::string ask_filepath()
{
    std::string filepath;

    std::cout << "Enter the path to the input file:\n";
    std::cin >> filepath;

    return filepath;
}

/// \brief Shows the list of the methods
void method_prompt()
{
    std::cout << "\nSelect a method to execute:\n";
    std::cout << "  Voronoi diagram:\n";
    std::cout << "    1. Fortune algorithm.\n";
    std::cout << "    2. Delaunay triangulation.\n";
    std::cout << "  Convex hull:\n";
    std::cout << "    3. Kale-Kirkpatrick method.\n";
    std::cout << "    4. Andrew and Jarvis method.\n";
    std::cout << "    5. Graham method.\n";
    std::cout << "    6. Fast recursive method.\n";
    std::cout << "\nInput the number of the method:\n";
}

/**
 * \brief The entry point of the program
 *
 * Can get a filepath for the file with nodes
 * from the shell. Otherwise, it will ask
 * the user to enter the filepath.
 */
int main(int argc, char *argv[]) {
    std::string filepath;
    int method_number;

    if (argc == 1) {
        filepath = ask_filepath();
    } else {
        filepath = argv[1];
    }

    load_nodes_from_file(filepath);

    method_prompt();
    std::cin >> method_number;
    switch (method_number) {
        case 1: {
            fortune();
            break;
        }
        case 3: {
            kirkpatrick();
            break;
        }
        case 5: {
            graham();
            break;
        }
        default: {
            std::cout << "Not implemented!";
            return 0;
        }
    }

    Render::open_window();
    while (Render::isOpen()) {
        Render::check_closing();
        Render::draw();
        Render::check_change_slide();
    }

    return 0;
}